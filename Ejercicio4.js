var i;
var j;

//Primer bucle, imprime el número i, siempre que sea igual o inferior a 9 y al finalizar le suma 1
for(i = 1; i <= 9; i++){

    //Segundo bucle, repite el número tantas veces como indique el valor del número i
    for (j = 1; j <= i; j++) {
        document.write(i);
    }
    // inserta un salto de línea para dividir los bloques de números
    document.write("<br>");

}